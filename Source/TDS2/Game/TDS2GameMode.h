// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS2GameMode.generated.h"

UCLASS(minimalapi)
class ATDS2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS2GameMode();
};



